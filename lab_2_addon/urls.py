from django.conf.urls import url
from .views import index

# Create your views here.
urlpatterns = [
    url(r'^$', index, name='index'),
]
